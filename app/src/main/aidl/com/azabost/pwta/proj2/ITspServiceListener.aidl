package com.azabost.pwta.proj2;

interface ITspServiceListener {
    oneway void onBestPermutationFound(in double distance, in int[] sequence, in double[] distances);
    oneway void onProgress(in double progress);
}
