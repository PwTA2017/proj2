package com.azabost.pwta.proj2;

import com.azabost.pwta.proj2.ITspServiceListener;
import com.azabost.pwta.proj2.Node;

interface ITspService {
    oneway void solveTspProblem(in ITspServiceListener listener, in List<Node> nodes, in int threads);
    oneway void stopSolving();
}
