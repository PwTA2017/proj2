package com.azabost.pwta.proj2

import android.Manifest
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import android.widget.SeekBar
import android.widget.Toast
import com.azabost.pwta.proj2.utils.logE
import com.azabost.pwta.proj2.utils.logI
import com.azabost.pwta.proj2.utils.logW
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity() {

    private val serviceListener = object : ITspServiceListener.Stub() {
        override fun onBestPermutationFound(distance: Double, sequence: IntArray?, distances: DoubleArray?) {
            this@MainActivity.logI("Best permutation: $distance. Sequence: ${sequence?.joinToString(", ")}")
            if (sequence != null && distances != null) {
                graphView.bestDistance = sequence
                bestSolution = Solution(sequence, distances)

                runOnUiThread {
                    solutionValue.text = distance.toString()
                }
            }
        }

        override fun onProgress(progress: Double) {
            runOnUiThread {
                if (progress < 1) {
                    progressValue.text = getString(R.string.percent_i, (progress * 100).roundToInt())
                } else {
                    progressValue.text = getString(R.string.percent_done)
                    updateButtonsWhenSolved()
                    printTime()
                }
            }
        }
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            this@MainActivity.logI("Service disconnected")
            tspService = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            this@MainActivity.logI("Service connected")

            tspService = ITspService.Stub.asInterface(service)
            binder = service

            service?.linkToDeath(deathRecipient, 0)
        }
    }

    private val deathRecipient = object : IBinder.DeathRecipient {
        override fun binderDied() {
            this@MainActivity.logW("Service died")
            tspService = null
            binder?.unlinkToDeath(this, 0)
        }
    }

    private var tspService: ITspService? = null
    private var binder: IBinder? = null
    private val nodes = mutableListOf<Node>()

    private data class Solution(val permutation: IntArray, val distances: DoubleArray)

    private var bestSolution: Solution? = null

    private val pickFileRequestCode = 1234
    private val writeExternalStoragePermissionRequest = 4321

    private var startTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initButtons()

        bindService(
                Intent(this, TspService::class.java),
                serviceConnection,
                Context.BIND_AUTO_CREATE)

        chooseFileButton.setOnClickListener { chooseFile() }

        saveButton.setOnClickListener {
            if (bestSolution != null) {
                saveToFile()
            }
        }

        threadsSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                threadsNumber.text = (progress + 1).toString()
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })

        solveButton.setOnClickListener {
            if (nodes.isNotEmpty()) {
                clearProgress()
                val threads = threadsNumber.text.toString().toIntOrNull() ?: 1

                logI("Requesting TSP solution for ${nodes.size} nodes with $threads threads")
                startTime = System.currentTimeMillis()

                tspService?.solveTspProblem(serviceListener, nodes, threads)
                updateButtonsWhenSolving()
            }
        }

        stopButton.setOnClickListener {
            tspService?.stopSolving()
            updateButtonsWhenSolved()
            printTime()
        }
    }

    private fun printTime() {
        val endTime = System.currentTimeMillis()
        val seconds = (endTime - startTime)/1000f
        logI("Solving time [s]: $seconds")
    }

    private fun chooseFile() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "*/*"
        }
        startActivityForResult(intent, pickFileRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == pickFileRequestCode && resultCode == Activity.RESULT_OK && data != null) {
            val uri = data.data
            onFileChosen(uri)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onFileChosen(uri: Uri) {
        val fileParser = TspFileParser(contentResolver, uri)
        val result = fileParser.read()

        if (result.nodes != null && result.error.isNullOrEmpty()) {
            nodes.clear()
            nodes.addAll(result.nodes)
            graphView.nodes = nodes
            updateButtonsWhenChosenGoodFile()
        } else {
            Toast.makeText(
                    this,
                    result.error ?: "Wrong file content",
                    Toast.LENGTH_LONG).show()
            graphView.nodes = emptyList()
            initButtons()
        }
        clearProgress()
    }

    private fun saveToFile() {
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    writeExternalStoragePermissionRequest)
        } else {
            bestSolution?.let { solution ->
                val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                val file = File(dir, UUID.randomUUID().toString() + ".txt")

                try {
                    dir.mkdirs()
                    FileOutputStream(file).bufferedWriter().use { writer ->
                        writer.write(solution.distances.size.toString())
                        writer.newLine()

                        for (i in 0 until solution.distances.size - 1) {
                            writer.write(solution.permutation[i].toString())
                            writer.write(" ")
                            writer.write(solution.permutation[i + 1].toString())
                            writer.write(" ")
                            writer.write(solution.distances[i].toString())
                            writer.newLine()
                        }

                        writer.write(solution.permutation.last().toString())
                        writer.write(" ")
                        writer.write(solution.permutation.first().toString())
                        writer.write(" ")
                        writer.write(solution.distances.last().toString())
                        writer.newLine()
                        writer.flush()
                    }

                    val msg = "Written to file: ${file.absolutePath}"
                    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
                } catch (e: Exception) {
                    val msg = "Couldn't write to file"
                    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
                    logE(msg, e)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == writeExternalStoragePermissionRequest
                && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            saveToFile()
        }
    }

    private fun clearProgress() {
        graphView.bestDistance = IntArray(0)
        progressValue.text = getString(R.string.percent_0)
        solutionValue.text = getString(R.string.value_0)
        bestSolution = null
    }

    private fun initButtons() {
        saveButton.isEnabled = false
        chooseFileButton.isEnabled = true
        solveButton.isEnabled = false
        stopButton.isEnabled = false
    }

    private fun updateButtonsWhenSolving() {
        saveButton.isEnabled = false
        chooseFileButton.isEnabled = false
        solveButton.isEnabled = false
        stopButton.isEnabled = true
    }

    private fun updateButtonsWhenSolved() {
        saveButton.isEnabled = true
        chooseFileButton.isEnabled = true
        solveButton.isEnabled = true
        stopButton.isEnabled = false
    }

    private fun updateButtonsWhenChosenGoodFile() {
        saveButton.isEnabled = false
        chooseFileButton.isEnabled = true
        solveButton.isEnabled = true
        stopButton.isEnabled = false
    }

    override fun onDestroy() {
        unbindService(serviceConnection)
        super.onDestroy()
    }
}
