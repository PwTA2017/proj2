package com.azabost.pwta.proj2

import android.content.ContentResolver
import android.net.Uri
import com.azabost.pwta.proj2.utils.logI
import java.io.BufferedReader
import java.io.InputStreamReader

class TspFileParser(private val contentResolver: ContentResolver, private val uri: Uri) {

    data class Result(
            val nodes: List<Node>? = null,
            val error: String? = null
    )

    private data class ValidationResult(
            val valid: Boolean = true,
            val error: String? = null
    )

    private val specificationParsers = listOf(TypeParser(), EdgeWeightTypeParser())

    fun read(): Result {
        var result = Result(error = "Wrong file format")

        BufferedReader(InputStreamReader(contentResolver.openInputStream(uri))).use { reader ->

            var line: String? = ""

            while (true) {
                line = reader.readLine()

                if (line != null) {
                    if (line.isBlank()) continue

                    val firstSpaceIndex = line.indexOfFirst { it == ' ' }

                    if (firstSpaceIndex > 0) {
                        val firstWord = line.take(firstSpaceIndex)

                        val parser = specificationParsers.firstOrNull { it.keyword == firstWord }

                        if (parser != null) {
                            val validationResult = parser.validate(line)
                            logI("${parser.javaClass} result: $validationResult")
                            if (!validationResult.valid) {
                                result = Result(error = validationResult.error)
                                break
                            }
                        } else if (NodeCoordParser.KEYWORD == firstWord) {
                            result = parseNodes(reader)
                            break
                        }
                    } else if (line == NodeCoordParser.KEYWORD) {
                        result = parseNodes(reader)
                        break
                    } else {
                        break
                    }
                } else {
                    break
                }
            }
        }

        return result
    }

    private fun parseNodes(reader: BufferedReader): Result {
        val nodeCoordParser = NodeCoordParser()
        val nodes = nodeCoordParser.getNodes(reader)

        return if (nodes != null && nodes.isNotEmpty()) {
            Result(nodes)
        } else {
            Result(error = "Nodes section could not be parsed.")
        }
    }

    private interface SpecificationEntryParser {
        val keyword: String
        fun validate(line: String): ValidationResult
    }

    private class TypeParser : SpecificationEntryParser {
        override val keyword = "TYPE"
        private val properValue = "TSP"

        override fun validate(line: String): ValidationResult {
            return if (line.takeLast(3) == properValue)
                ValidationResult()
            else
                ValidationResult(false, "$keyword must be $properValue")
        }
    }

    private class EdgeWeightTypeParser : SpecificationEntryParser {
        override val keyword = "EDGE_WEIGHT_TYPE"
        private val properValue = "EUC_2D"

        override fun validate(line: String): ValidationResult {
            return if (line.takeLast(6) == properValue)
                ValidationResult()
            else
                ValidationResult(false, "$keyword must be $properValue")
        }
    }

    private class NodeCoordParser {
        fun getNodes(reader: BufferedReader): List<Node>? {
            val nodes = mutableListOf<Node>()

            var line: String? = ""
            var currentNodeNumber = 0

            while (true) {
                line = reader.readLine()

                if (line != null && line != "EOF") {
                    if (line.isBlank()) continue

                    currentNodeNumber += 1

                    val numbersStrings = line.split(" ")

                    // Each row should contain 3 numbers
                    if (numbersStrings.size != 3) return null

                    // First number should be the node index (starting from 1)
                    val nodeNumber = numbersStrings[0].toIntOrNull() ?: return null
                    if (nodeNumber != currentNodeNumber) return null

                    val x = numbersStrings[1].toDoubleOrNull() ?: return null
                    val y = numbersStrings[2].toDoubleOrNull() ?: return null

                    nodes += Node(x, y)
                } else {
                    break
                }
            }

            return nodes
        }

        companion object {
            val KEYWORD = "NODE_COORD_SECTION"
        }
    }

}