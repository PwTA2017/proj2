package com.azabost.pwta.proj2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class GraphView @JvmOverloads constructor(
        context: Context,
        attributeSet: AttributeSet? = null,
        defStyleAttr: Int = 0,
        defStyleRes: Int = 0

) : View(context, attributeSet, defStyleAttr, defStyleRes) {

    var nodes: List<Node> = emptyList()
        set(value) {
            field = value
            calculateCanvasNodes()
            invalidate()
        }

    var bestDistance = IntArray(0)
        set(value) {
            field = value
            invalidate()
        }

    private val canvasNodes: MutableList<Node> = mutableListOf()

    private val nodePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        setARGB(255, 255, 0, 0)
    }

    private val nodeIndexPain = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        setARGB(255, 0, 0, 0)
        textSize = 50f
    }

    private val nodeRadius = 10F

    private fun calculateCanvasNodes() {
        canvasNodes.clear()

        val minX = nodes.minBy { it.x }?.x
        val minY = nodes.minBy { it.y }?.y
        val maxX = nodes.maxBy { it.x }?.x
        val maxY = nodes.maxBy { it.y }?.y

        if (minX != null && maxX != null && minY != null && maxY != null) {

            val realMinX = minX - 0.2 * (maxX - minX)
            val realMaxX = maxX + 0.2 * (maxX - minX)
            val realMinY = minY - 0.2 * (maxY - minY)
            val realMaxY = maxY + 0.2 * (maxY - minY)

            nodes.forEach {
                val x = (it.x - realMinX) * width / (realMaxX - realMinX)
                val y = height - (it.y - realMinY) * height / (realMaxY - realMinY)
                canvasNodes.add(Node(x, y))
            }
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvasNodes.forEachIndexed { index, node ->
            canvas.drawCircle(
                    node.x.toFloat(),
                    node.y.toFloat(),
                    nodeRadius,
                    nodePaint
            )
            canvas.drawText(
                    (index + 1).toString(),
                    node.x.toFloat() - 50f,
                    node.y.toFloat() - 20f,
                    nodeIndexPain
            )
        }

        if (bestDistance.isNotEmpty()) {
            for (i in 0 until bestDistance.lastIndex) {
                canvas.drawLine(
                        canvasNodes[bestDistance[i]].x.toFloat(),
                        canvasNodes[bestDistance[i]].y.toFloat(),
                        canvasNodes[bestDistance[i + 1]].x.toFloat(),
                        canvasNodes[bestDistance[i + 1]].y.toFloat(),
                        nodePaint
                )
            }

            canvas.drawLine(
                    canvasNodes[bestDistance[0]].x.toFloat(),
                    canvasNodes[bestDistance[0]].y.toFloat(),
                    canvasNodes[bestDistance[bestDistance.lastIndex]].x.toFloat(),
                    canvasNodes[bestDistance[bestDistance.lastIndex]].y.toFloat(),
                    nodePaint
            )
        }

    }

}