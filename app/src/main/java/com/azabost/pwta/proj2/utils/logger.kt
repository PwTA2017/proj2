package com.azabost.pwta.proj2.utils

import android.util.Log

inline fun <reified T> T.logD(msg: String) = Log.d(T::class.java.simpleName, msg)
inline fun <reified T> T.logI(msg: String) = Log.i(T::class.java.simpleName, msg)
inline fun <reified T> T.logW(msg: String) = Log.w(T::class.java.simpleName, msg)
inline fun <reified T> T.logE(msg: String, throwable: Throwable) =
        Log.e(T::class.java.simpleName, msg, throwable)

