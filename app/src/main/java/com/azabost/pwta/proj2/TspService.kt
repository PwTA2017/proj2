package com.azabost.pwta.proj2

import android.app.Service
import android.content.Intent
import android.os.*
import com.azabost.pwta.proj2.utils.logI
import com.azabost.pwta.proj2.utils.logW

class TspService : Service() {

    private var listener: ITspServiceListener? = null
    private var listenerBinder: IBinder? = null
    private val nodes: MutableList<Node> = mutableListOf()
    private var distances: Array<DoubleArray> = emptyArray()
    private var bestDistance: Double = 0.0
    private val bestPermutation: MutableList<Int> = mutableListOf()
    private var currentPermutationsCount = 0L
    private var allPermutationsCount = 0L
    private var lastProgress = 0.0
    private var solvingHandlers = mutableListOf<SolvingHandler>()
    private val publicationThread = HandlerThread("PublicationThread").apply { start() }
    private val publicationHandler = PublicationHandler(publicationThread.looper)
    private var jobs = mutableListOf<List<Int>>()

    companion object HandlerArgs {
        val MSG_TAKE_JOB = 1
        val MSG_PROCESS_JOB = 2
        val MSG_STOP = 3
        val MSG_INC_PERMUTATIONS_COUNT = 4
        val MSG_PUBLISH_BEST = 5
        val DISTANCE = "distance"
        val PERMUTATION = "permutation"
    }

    private var listenerDeath = object : IBinder.DeathRecipient {
        override fun binderDied() {
            this@TspService.logW("Listener died")
            listener = null
            listenerBinder?.unlinkToDeath(this, 0)
        }
    }

    private val binder = object : ITspService.Stub() {
        override fun solveTspProblem(listener: ITspServiceListener?, nodes: List<Node>?, threads: Int) {
            this@TspService.listener = listener
            listener?.asBinder()?.linkToDeath(listenerDeath, 0)

            if (nodes != null) {
                this@TspService.nodes.clear()
                this@TspService.nodes.addAll(nodes)

                val realThreads = if (threads > 0) threads else 1

                solveTsp(realThreads)
            }
        }

        override fun stopSolving() {
            stopHandlers()
        }
    }

    fun solveTsp(threads: Int) {
        stopHandlers()

        logI("Solving TSP using $threads threads")

        calculateDistances()

        allPermutationsCount = 1
        for (i in 1 until nodes.size) {
            allPermutationsCount *= i
        }

        logI("Predicted permutations: $allPermutationsCount")

        val initialPermutation = IntRange(0, nodes.lastIndex).toList()

        bestDistance = getPermutationDistance(initialPermutation)
        logI("Initial permutation distance: $bestDistance")

        bestPermutation.clear()
        bestPermutation.addAll(initialPermutation)

        listener?.onBestPermutationFound(
                bestDistance,
                bestPermutation.toIntArray(),
                getPermutationDistancesList(bestPermutation).toDoubleArray())

        currentPermutationsCount = 1
        reportProgress()
        currentPermutationsCount = 0
        lastProgress = 0.0

        jobs.clear()

        for (i in 1..nodes.lastIndex) {
            val initialHandlerPermutation = mutableListOf<Int>().apply {
                addAll(initialPermutation)
                swap(1, i)
            }

            jobs.add(initialHandlerPermutation)
        }

        for (i in 1..threads) {
            val threadName = "SolvingThread-$i"
            logI("Starting thread: $threadName")
            val thread = HandlerThread(threadName)
            thread.start()
            val handler = SolvingHandler(thread.looper, nonPermutableHeadSize = 2)
            handler.sendEmptyMessage(MSG_TAKE_JOB)
            solvingHandlers.add(handler)
        }
    }

    fun stopHandlers() {
        solvingHandlers.filter { it.looper.thread.isAlive }.forEach {
            try {
                it.sendEmptyMessage(MSG_STOP)
            } catch (e: IllegalStateException) {
            }
        }
        solvingHandlers.clear()
    }

    inner class PublicationHandler(looper: Looper) : Handler(looper) {

        override fun handleMessage(msg: Message?) {
            when (msg?.what) {
                MSG_INC_PERMUTATIONS_COUNT -> publishProgress(msg)
                MSG_PUBLISH_BEST -> publishBest(msg)
            }
        }

        private fun publishProgress(msg: Message) {
            currentPermutationsCount += msg.obj as Long
            reportProgress()
        }

        private fun publishBest(msg: Message) {
            val permutation = msg.data.getIntegerArrayList(PERMUTATION)
            val distance = msg.data.getDouble(DISTANCE)

            if (distance < bestDistance) {
                bestDistance = distance
                bestPermutation.clear()
                bestPermutation.addAll(permutation)

                listener?.onBestPermutationFound(
                        bestDistance,
                        bestPermutation.toIntArray(),
                        getPermutationDistancesList(bestPermutation).toDoubleArray())
            }
        }
    }

    inner class SolvingHandler(
            looper: Looper,
            private val nonPermutableHeadSize: Int = 0) : Handler(looper) {

        private val permutation = mutableListOf<Int>()
        private val nonPermutableHead = mutableListOf<Int>()
        private var stop = false
        private var localBestDistance: Double = 0.0
        private var localPermutationsCount = 0L
        private val permutationCountTreshold = allPermutationsCount / 1000

        override fun handleMessage(msg: Message?) {

            when (msg?.what) {
                MSG_STOP -> stop()
                MSG_TAKE_JOB -> takeJob()
                MSG_PROCESS_JOB -> processJob()
            }
        }

        private fun takeJob() {
            if (stop) return

            permutation.clear()
            nonPermutableHead.clear()

            var job: List<Int>? = null

            synchronized(this@TspService) {
                if (jobs.isNotEmpty()) {
                    job = jobs.removeAt(0)
                }
            }

            val immutableJob = job
            if (immutableJob != null) {
                permutation.addAll(immutableJob.drop(nonPermutableHeadSize))
                nonPermutableHead.addAll(immutableJob.take(nonPermutableHeadSize))

                val jobHeader = nonPermutableHead.joinToString(", ")
                val jobBody = permutation.joinToString(", ")
                logI("Job taken: $jobHeader, $jobBody")
            }

            if (permutation.isNotEmpty()) {
                sendEmptyMessage(MSG_PROCESS_JOB)
            } else {
                stop()
            }
        }

        private fun stop() {
            logI("Stopping thread ${looper.thread.name}")
            stop = true
            looper.quit()
        }

        private fun processJob() {
            val jobHeader = nonPermutableHead.joinToString(", ")
            val jobBody = permutation.joinToString(", ")
            logI("Processing job: $jobHeader, $jobBody")

            localBestDistance = bestDistance

            val startTime = System.currentTimeMillis()
            heapPermute(permutation.size)
            val endTime = System.currentTimeMillis()

            publishPermutations()

            val seconds = (endTime - startTime) / 1000f
            logI("Job done: $jobHeader, $jobBody. Time [s]: $seconds")

            takeJob()
        }

        private fun heapPermute(n: Int) {
            if (stop) return

            if (hasMessages(MSG_STOP)) stop()

            if (n > 1) {
                for (i in 0 until n) {
                    heapPermute(n - 1)

                    if (n % 2 == 0) {
                        permutation.swap(i, n - 1)
                    } else {
                        permutation.swap(0, n - 1)
                    }
                }
            } else {
                val fullPermutation = nonPermutableHead + permutation
                val d = getPermutationDistance(fullPermutation)
                val epsilon = Math.ulp(d)

                if (d + epsilon < localBestDistance) {
                    localBestDistance = d
                    val perm = ArrayList<Int>()
                    perm.addAll(fullPermutation)
                    val data = Bundle().apply {
                        putIntegerArrayList(PERMUTATION, perm)
                        putDouble(DISTANCE, localBestDistance)
                    }
                    val message = publicationHandler.obtainMessage()?.apply {
                        setData(data)
                        what = MSG_PUBLISH_BEST
                    }
                    publicationHandler.sendMessage(message)
                }

                localPermutationsCount += 1
                if (localPermutationsCount == permutationCountTreshold) {
                    publishPermutations()
                }

            }
        }

        private fun publishPermutations() {
            val message = publicationHandler.obtainMessage(MSG_INC_PERMUTATIONS_COUNT, localPermutationsCount)
            publicationHandler.sendMessage(message)
            localPermutationsCount = 0
        }
    }

    private fun reportProgress() {
        if (currentPermutationsCount == allPermutationsCount) {
            val progress = 1.0
            reportProgress(progress)
        } else {
            val progress = currentPermutationsCount.toDouble() / allPermutationsCount.toDouble()

            if (progress > lastProgress + 0.01 || progress == 1.0) {
                reportProgress(progress)
            }
        }
    }

    private fun reportProgress(progress: Double) {
        lastProgress = progress
        listener?.onProgress(progress)
    }

    private fun <T> MutableList<T>.swap(i: Int, j: Int) {
        val temp = this[i]
        this[i] = this[j]
        this[j] = temp
    }

    private fun getPermutationDistance(permutation: List<Int>): Double {
        var sum = 0.0

        for (i in 0 until permutation.size - 1) {
            sum += distances[permutation[i]][permutation[i + 1]]
        }
        sum += distances[permutation[0]][permutation.last()]
        return sum
    }

    private fun getPermutationDistancesList(permutation: List<Int>): List<Double> {
        val result = mutableListOf<Double>()

        for (i in 0 until permutation.size - 1) {
            result += distances[permutation[i]][permutation[i + 1]]
        }
        result += distances[permutation[0]][permutation.last()]

        return result
    }

    private fun calculateDistances() {
        logI("Calculating distances between nodes")
        distances = Array<DoubleArray>(nodes.size, { DoubleArray(nodes.size, { 0.0 }) })

        for (i in 0 until nodes.lastIndex) {
            for (j in i + 1..nodes.lastIndex) {
                val nodeI = nodes[i]
                val nodeJ = nodes[j]
                val x1x2 = nodeI.x - nodeJ.x
                val y1y2 = nodeI.y - nodeJ.y
                val d = Math.sqrt((x1x2 * x1x2 + y1y2 * y1y2))
                distances[i][j] = d
                distances[j][i] = d
            }

        }
        logI("Distances calculated")
    }

    override fun onBind(intent: Intent): IBinder? {
        return binder
    }
}
