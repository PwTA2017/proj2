Zawartość:
- examples/ - katalog zawierający przykładowe pliki wejściowe w formacie TSP
- app/src/main/java/com/azabost/pwta/proj2/MainActivity.kt - główna (i jedyna) aktywność
- app/src/main/java/com/azabost/pwta/proj2/Node.kt - klasa zawierająca współrzędne wierzchołka grafu
- app/src/main/java/com/azabost/pwta/proj2/GraphView.kt - widok podglądu grafu
- app/src/main/java/com/azabost/pwta/proj2/TspService.kt - implementacja serwisu
- app/src/main/java/com/azabost/pwta/proj2/TspFileParser.kt - klasa służąca do parsowania pliku wejściowego
- app/src/main/java/com/azabost/pwta/proj2/utils/logger.kt - funkcje pomocnicze dotyczące logowania
- app/src/main/aidl/com/azabost/pwta/proj2/ITspService.aidl - interfejs serwisu
- app/src/main/aidl/com/azabost/pwta/proj2/ITspServiceListener.aidl - interfejs klienta serwisu
- app/src/main/aidl/com/azabost/pwta/proj2/Node.aidl - definicja klasy Node